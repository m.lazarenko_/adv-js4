
// AJAX - це комбінація технологій, яка дозволяє отримувати та надсилати данні (оновлювати контент на сторінці) без перезавантажень сторінки. Наприклад, база клієнтів сайту, їх логіни та особисті данні.



let result = fetch('https://ajax.test-danit.com/api/swapi/films');

result
    .then((result) => result.json())
    .then((data) => {
        console.log(data)
        data.forEach(element => {
            let list = document.createElement("ul");
            document.body.append(list);
            let listItem = document.createElement("li");
            listItem.textContent = `Episode: ${element.episodeId}. ${element.name}. ${element.openingCrawl}`;
            list.append(listItem);
            element.characters.forEach((item) =>
                fetch(item)
                    .then((result) => result.json())
                    .then((pr) => {
                        let characterList = document.createElement('ul');
                        listItem.append(characterList);
                        let charactersItem = document.createElement('li');
                        charactersItem.textContent = pr.name
                        characterList.append(charactersItem)
                    })
            )
            
        });
    })